package pl.gdansk.best.hrms;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.gdansk.best.hrms.dao.PersonDAOImpl;

@Controller
public class HomeController {
    @Autowired
    private PersonDAOImpl personDao;

    @RequestMapping(value={"/"})
    public ModelAndView home() {
        List listPersons = this.personDao.list();
        ModelAndView model = new ModelAndView("home");
        model.addObject("personList", (Object)listPersons);
        return model;
    }
}