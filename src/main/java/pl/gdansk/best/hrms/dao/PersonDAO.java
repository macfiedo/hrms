package pl.gdansk.best.hrms.dao;

import java.util.List;
import pl.gdansk.best.hrms.model.Person;

public interface PersonDAO {
    public List<Person> list();
}