package pl.gdansk.best.hrms.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.ResultTransformer;
import org.springframework.transaction.annotation.Transactional;
import pl.gdansk.best.hrms.model.Person;

public class PersonDAOImpl {
    private SessionFactory sessionFactory;

    public PersonDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public List<Person> list() {
        List listPerson = this.sessionFactory.getCurrentSession().createCriteria((Class)Person.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        return listPerson;
    }
}