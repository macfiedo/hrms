package pl.gdansk.best.hrms.model;

import java.sql.Date;

public class Person {
    private int id;
    private int parentId;
    private String firstName;
    private String lastName;
    private String membershipLevel;
    private String joiningDate;
    private String babyMemberDate;
    private String fullMemberDate;
    private String alumniDate;
    private String inactiveDate;
    private String workingGroupFirst;
    private String workingGroupSecond;
    private Date birthDate;
    private String email;
    private String phone;
    private String badgeNameLocal;
    private String badgeNameEnglish;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return this.parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMembershipLevel() {
        return this.membershipLevel;
    }

    public void setMembershipLevel(String membershipLevel) {
        this.membershipLevel = membershipLevel;
    }

    public String getJoiningDate() {
        return this.joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getBabyMemberDate() {
        return this.babyMemberDate;
    }

    public void setBabyMemberDate(String babyMemberDate) {
        this.babyMemberDate = babyMemberDate;
    }

    public String getFullMemberDate() {
        return this.fullMemberDate;
    }

    public void setFullMemberDate(String fullMemberDate) {
        this.fullMemberDate = fullMemberDate;
    }

    public String getAlumniDate() {
        return this.alumniDate;
    }

    public void setAlumniDate(String alumniDate) {
        this.alumniDate = alumniDate;
    }

    public String getInactiveDate() {
        return this.inactiveDate;
    }

    public void setInactiveDate(String inactiveDate) {
        this.inactiveDate = inactiveDate;
    }

    public String getWorkingGroupFirst() {
        return this.workingGroupFirst;
    }

    public void setWorkingGroupFirst(String workingGroupFirst) {
        this.workingGroupFirst = workingGroupFirst;
    }

    public String getWorkingGroupSecond() {
        return this.workingGroupSecond;
    }

    public void setWorkingGroupSecond(String workingGroupSecond) {
        this.workingGroupSecond = workingGroupSecond;
    }

    public Date getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBadgeNameLocal() {
        return this.badgeNameLocal;
    }

    public void setBadgeNameLocal(String badgeNameLocal) {
        this.badgeNameLocal = badgeNameLocal;
    }

    public String getBadgeNameEnglish() {
        return this.badgeNameEnglish;
    }

    public void setBadgeNameEnglish(String badgeNameEnglish) {
        this.badgeNameEnglish = badgeNameEnglish;
    }
}