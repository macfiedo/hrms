DROP TABLE person;

CREATE TABLE person(
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  firstName VARCHAR(100) NOT NULL,
  lastName VARCHAR(100) NOT NULL,
  membershipLevel VARCHAR(20) NOT NULL,
  joiningDate VARCHAR(100),
  babyMemberDate VARCHAR(100),
  fullMemberDate VARCHAR(100),
  alumniDate VARCHAR(100),
  inactiveDate VARCHAR(100),
  workingGroupFirst VARCHAR(10),
  workingGroupSecond VARCHAR(10),
  parentId INT,
  birthDate DATE,
  email VARCHAR(100),
  phone VARCHAR(20),
  badgeNameLocal VARCHAR(100),
  badgeNameEnglish VARCHAR(100)
);
INSERT INTO person VALUES (1, 'Maciej', 'Fiedorowicz', 'full', '', '' ,'' ,'' , '', '', '', 1, '1992-05-01', '', '' ,'', '');